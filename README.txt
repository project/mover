// $Id$

-- SUMMARY --

The Mover modules provide the ability to effortlessly move content,
currently users and nodes (with associated taxonomy), between Drupal sites.
This is especially useful for content migration, and provides extensive
opportunities for other modules to extend Mover for altering content as
it is imported. The module currently supports Drupal 6, but should not
be too difficult to update soon to support Drupal 7.

This module has similarities to other migration modules, and is most
similar to Node export which was the foundation for it's creation,
but this module is significantly different than others. One major
difference from Node export is that this module writes to separate text
files for each node and user, which is better for PHP memory usage
(although browsing directories of thousands of the exported files can be
slow). Another difference is that Node export doesn't migrate users.

This module can be used by anyone for very simple content moving operations
(such as between staging and production on a site), but the real power is
the very flexible API for very large and advanced content migrations
between completely different Drupal installations.

Development sponsored by Digital Deployment
Based on work from Node export by danielb, James Andres, and chia


-- REQUIREMENTS --

* The Path Redirect module is currently required and used for importing
  URL aliases from the previous installation as 301 redirects on the new.


-- INSTALLATION --

You will need to install the Mover module on both the old site and the new site:

* Copy the mover module directory to your sites/all/modules directory, so it
  is located in sites/all/modules/mover/.

* Enable either the Mover Export module or the Mover Import module, depending
  on what you need.

* Go to Content Management/Content export or Content Management/Content import
  to begin running migration operations.

-- API --

Please read the code to fully understand how the API works. The module is under
active development so keeping up with detailed documentation would be difficult.

Here's how to get started.

* If you need to modify the exported data as it is exported, look for
  drupal_alter('mover_node', ...) and drupal_alter('mover_user', ...)
  in mover_export.module and create a function in your custom module following
  the function signature described in mover_export.module

* If you need to modify data as it is imported, look for the node_invoke_nodeapi
  calls in mover_import.module. Most of time this is the easiest way to modify
  node objects before the new node is saved. There are several nodeapi calls
  for each new node, as taxnomomy vocabularies are terms are processed first
  these calls expect your hook to return a value if you want to specific
  vocubulary or term mapping. There is also a userapi hook system (that doesn't
  exist in Drupal, but was created for this module), and it works just like
  nodeapi with hooks for user specific actions, such as mapping user roles.
  There are also drupal_alter calls in mover_import.module, but they aren't as
  useful as nodeapi calls because they don't return values.

-- CONTACT --

Current maintainer:
* Ben Shell (benshell)
